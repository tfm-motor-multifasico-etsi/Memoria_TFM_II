\chapter{Descripción de la bancada}\label{ch_2}

En este capítulo se describirá de forma somera la bancada sobre la que se han
realizado los análisis y experimentos del trabajo.


\section{Motor de cinco fases}

El motor de cinco fases es, en realidad, un motor de tres fases de Siemens y
alterado por Mayre. Tiene una potencia nominal de $1.4~\si{\kilo\watt}$ y una
velocidad nominal de $1000~\si{rpm}$\autocite{Sistema}. Se puede ver en la
\autoref{fig:ch_2:cinco_fases} como las cinco fases están en estrella, habiendo
un
neutro al que se conectan todas las fases.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{
		ch_2/figures/DSC_0044
	}
	\caption{
		Motor
	}\label{fig:ch_2:cinco_fases}
\end{figure}

\section{Motor de continua}

Para simular cargas sobre el motor multifásico se dispone de una máquina de
continua modelo \tt{DMP112-2LA} de ABB, que se muestra en la
\autoref{fig:ch_2:mot_cont}. Según su placa de características, disponible en
la \autoref{fig:ch_2:placa}, el motor tiene un rango de tensiones entre
$180~\si{\volt}$ y $360~\si{\volt}$ y puede desarrollar hasta
$3.2~\si{\kilo\watt}$
\autocite{Dc}.

\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth,
			height=0.25\textheight]{ch_2/figures/DSC_0049}
		\caption{Motor de continua
		}\label{fig:ch_2:mot_cont}
	\end{subfigure}
	\begin{subfigure}[b]{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth, height=0.25\textheight]{ch_2/figures/placa}
		\caption{Placa de datos del motor de continua
		}\label{fig:ch_2:placa}
	\end{subfigure}
	\caption{Motor de continua}\label{fig:ch2:maq_cont}
\end{figure}

\section{Encoder}

El encoder Hohner \tt{10-11657-2500}, que se puede ver en la
\autoref{fig:ch_2:encoder}, sirve para calcular la velocidad y la posición.

El encoder se alimenta con una tensión de continua de $5~\si{\volt}$ y tiene una
resolución de $2500$ impulsos \autocite{encoder}.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/DSC_0047}
	\caption{Encoder
	}\label{fig:ch_2:encoder}
\end{figure}

\section{DC-Link}

Para generar la tensión de continua que alimenta a los inversores que se
describirán más adelante en la \autoref{sec:ch_2:inver}, se utiliza la fuente de
tensión \tt{KDC 300-50}, cuya imagen de catálogo se muestra en
\autoref{fig:ch_2:fuente_tensión}.

Esta fuente, que permite el uso de varias en paralelo, puede desarrollar una
potencia máxima de $15~\si{\kilo\watt}$ con una tensión máxima de
$300~\si{\volt}$ y una corriente de $50~\si{\ampere}$ \autocite{argantix}. Esta
potencia es suficiente para alimentar al motor y para realizar arranques.

\begin{figure}[H]
	\centering
	\includegraphics[height=0.2\textheight]{ch_2/figures/argantix}
	\caption{Fuente de tensión\cite{argantix}
	}\label{fig:ch_2:fuente_tensión}
\end{figure}


\section{Armario del motor multifásico}

Los inversores y la electrónica de control de los mismos están el
armario de la \autoref{fig:ch_2:arm_mot}
\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/DSC_0053}
	\caption{Armario del motor multifásico
	}\label{fig:ch_2:arm_mot}
\end{figure}

\subsection{Inversores}\label{sec:ch_2:inver}

Se emplean dos inversores trifásicos \tt{SKS 22F B6u + E1CIF + B6CI 13
	V12} para generar la corriente de cinco fases necesaria para el motor. Se puede
ver una fotografía de uno de ellos en la \autoref{fig:ch_2:DSC_0056}.

Incorporan sensores de corriente de efecto Hall, carga suave de condensadores
automática, breakers y monitorización de la tensión del condensador de
continua. También pueden actuar como convertidores AC/AC Back to Back, pero no
se usan de este modo en la bancada.

La corriente máxima es de $22~\si{\ampere}$, con una tensión continua máxima
de $750~\si{\volt}$. El sistema puede conmutar con una frecuencia de hasta
$10~\si{\kilo\hertz}$ \autocite{inverter}.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.3\textheight]{
		ch_2/figures/DSC_0056
	}
	\caption{
		Inversores.
	}\label{fig:ch_2:DSC_0056}
\end{figure}

\subsection{DSP}

El control de los inversores se ejecuta desde un DSP modelo
\tt{TMS320F28335}, apoyándose en placas de adaptación de señal. Además,
desde el mismo DSP también se pueden controlar varios interruptores para
simular faltas.\@ Se puede ver la placa de adaptación y el DSP en la
\autoref{fig:ch_2:DSC}. Su manual se puede encontrar en \autocite{Texas}.

El DSP contiene varios módulos muy convenientes, como los módulos de
ADC, PWM y EQEP, que facilitan la adquisición de señales y la generación de
pulsos para los inversores. Hay más información sobre cómo funciona el módulo
de EQEP en el \autoref{ch_3}.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.3\textheight]{ch_2/figures/DSC_0057}
	\caption{DSP}\label{fig:ch_2:DSC}
\end{figure}

\subsection{Protecciones}

En el mismo armario se han incorporado las protecciones magneto-térmicas y
diferenciales necesarias y varios bloques de interruptores que sirven para
simular faltas. Estos elementos se pueden observar en la
\autoref{fig:ch_2:protecc}.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/protecc}
	\caption{Protecciones del armario del motor
		multifásico.
	}\label{fig:ch_2:protecc}
\end{figure}

\section{Armario del motor de corriente continua}

El armario de la \autoref{fig:ch_2:arm_cont} contiene los equipos necesarios
para utilizar el motor de continua.

\begin{figure}[ht]
	\begin{subfigure}[b]{0.2\textwidth}
		\centering
		\includegraphics[   height=0.2\textheight,
			width=0.4\textwidth]{ch_2/figures/DSC_0050}
		\caption{Armario, frente}\label{fig:ch_2:arm_var_frente}
	\end{subfigure}
	\begin{subfigure}[b]{0.2\textwidth}
		\centering
		\includegraphics[   height=0.2\textheight,
			width=0.4\textwidth]{ch_2/figures/DSC_0052}
		\caption{Armario, lateral}\label{fig:ch_2:arm_var_lat}
	\end{subfigure}
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[height=0.2\textheight]{ch_2/figures/DSC_0051}
		\caption{Armario, botonera}\label{fig:ch_2:arm_var_bot}
	\end{subfigure}
	\caption{
		Exterior del armario de continua
	}\label{fig:ch_2:arm_cont}
\end{figure}

\subsection{Variador}
Se emplea un rectificador controlado que forma parte de un variador modelo
\tt{DMV 2342} de la marca Leroy-Somer (\autoref{fig:ch_2:variador}) para
controlar el par producido por el motor de corriente continua.

El rectificador toma tensiones de red de $220~\si{\volt}$ a $480~\si{\volt}$
y genera una tensión de continua de entre $260~\si{\volt}$ y $530~\si{\volt}$
con corrientes máximas que varían $1850~\si{\ampere}$ y $25~\si{\ampere}$. Tiene
una potencia máxima de $750~\si{\kilo\watt}$\autocite{variador}, que  es mucho
mayor que la potencia de la máquina de continua, que el accionamiento
multifásico e incluso que la fuente de continua; el variador está
sobredimensionado.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/DSC_0065}
	\caption{Variador}\label{fig:ch_2:variador}
\end{figure}

\subsection{Protecciones}

Para poner el motor en marcha hay una redundancia múltiple de interruptores.
Hay un interruptor, un contactor manual y una llave. Si cualquiera de estos
interruptores no está en la posición adecuada, no es posible poner en marcha el
variador. Todo es necesario porque el rectificador puede mover mucha potencia.
También dispone de protecciones magneto-térmicas y diferenciales. En la
\autoref{fig:ch_2:protec_var} se pueden ver las distintas protecciones e
interruptores del equipo.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/DSC_0063}
	\caption{
		Protecciones e interruptores
	}\label{fig:ch_2:protec_var}
\end{figure}

\section{Ordenador}

El ordenador tiene un sistema operativo Windows 10, de 64 bits. En la
\autoref{fig:ch_2:ordenador_nuevo} se pueden ver que tiene $16~\si{\giga\byte}$
de memoria RAM, que tiene un procesador Intel i7 de séptima generación y la
subversión de Windows 10 que tiene instalada. Esta subversión está congelada,
ya que Microsoft está retirando soporte a algunas aplicaciones de sistemas muy
antiguos. Además, para prevenir que intente actualizarse independientemente, se
ha desconectado de internet. La ficha técnica del procesador está disponible
en~\cite{processor2}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_2/figures/ordenador_nuevo
	}
	\caption{
		Especificaciones del ordenador utilizado.
	}\label{fig:ch_2:ordenador_nuevo}
\end{figure}

\section{MentorSoft}


Para manejar el variador se utiliza el programa \tt{MentorSoft}. Permite
controlar y modificar los parámetros del regulador que contiene el variador que
a su vez regulan el par y la velocidad del motor de continua. La interfaz
inicial del programa es la de la \autoref{fig:ch_2:msoft}.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.25\textheight]{ch_2/figures/msoft}
	\caption{MentorSoft
	}\label{fig:ch_2:msoft}
\end{figure}

\section{DMC Developer Pro}

El compilador cruzado \tt{DMC28x Developer Pro} sirve para compilar
programas desde el ordenador para el DSP.\@ Permite el uso y generación de scripts
que facilitan el enlazado de librerías y la compilación. Se muestra en la
\autoref{fig:ch_2:dmc}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{ch_2/figures/dmc}
	\caption{DMC Developer Pro}\label{fig:ch_2:dmc}
\end{figure}

\section{Monitor}

El programa monitor DSP es una herramienta de elaboración propia del
departamento para facilitar la interacción con el DSP.\@ Con él se pueden alterar
las variables globales, extraer los registros del DSP, cargar los programas y
poner en marcha y parar el motor. Se puede ver en marcha en la
\autoref{fig:ch_2:monitor.PNG}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\textwidth]{
		ch_2/figures/monitor.PNG
	}
	\caption{
		Monitor DSP
	}\label{fig:ch_2:monitor.PNG}
\end{figure}
\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
