/*=======================================================================================================================
    eQEP for Low-Speed and High-Speed Measuring with Capture Edge Unit and Unit
Time Out methods
=======================================================================================================================*/
// clang-format off

#define  UPEVENTDIV1   0x00
#define  UPEVENTDIV2   0x01
#define  UPEVENTDIV4   0x02
#define  UPEVENTDIV8   0x03
#define  UPEVENTDIV16  0x04

#define  SYSCLKDIV1    0x00
#define  SYSCLKDIV2    0x01
#define  SYSCLKDIV4    0x02
#define  SYSCLKDIV8    0x03
#define  SYSCLKDIV16   0x04
#define  SYSCLKDIV32   0x05
#define  SYSCLKDIV64   0x06
#define  SYSCLKDIV128  0x07

#define  VELOCFACTORX1  (float) 94247.77961
#define  VELOCFACTORX2  (float) 188495.5592
#define  VELOCFACTORX4  (float) 376991.1184
#define  VELOCFACTORX8  (float) 753982.2369
#define  VELOCFACTORX16 (float) 1508000

#define  VSTEPMAX (float) 0.2

#define  QCPRDMAX 40000L
#define  QCPRDMIN 10000L

#define  wm_MAX    (float) 110 // rad/seg

//#define  EQEP_TIME_OUT_PERIOD     10000L                   // Unit Out Time Period
//#define  EQEP_TIME_OUT_PERIOD     15000L                   // Unit Out Time Period
#define  EQEP_TIME_OUT_PERIOD     20000L                   // Unit Out Time Period
//#define  EQEP_TIME_OUT_PERIOD     30000L                   // Unit Out Time Period  usado en la era Blasista
//#define  EQEP_TIME_OUT_PERIOD     60000L                   // Unit Out Time Period
//#define  EQEP_TIME_OUT_PERIOD     90000L                   // Unit Out Time Period
//#define  EQEP_TIME_OUT_PERIOD     180000L                  // Unit Out Time Period

#define  QEPCNTMAX     10000L               // EQEP Counter Maximum Value

#define  VELOCK (float) 94247.77961

// PASAN A SER GLOBALES MRA 02-2023
//float  eqeptmr    = 1;
//float  hdx        = 0;
float wm_kp1max = 0;
float wm_kp1min = 0;
Uint16 chgflag    = 0;

int stvel        = 2;

// Para el método 1 de medir velocidad:

//float  velocfactor    = 753982.2369;            // for X = 8
//float  velocfactor    = 376991.1184;            // for X = 4
float  velocfactor    = 188495.5592;                // for X = 2
//float  velocfactor    = 94247.77961;            // for X = 1

// Para el método 2 de medir velocidad:

//float  velocfactor2    = 753982.2369;            // for X = 8
//float  velocfactor2    = 376991.1184;            // for X = 4
float  velocfactor2    = 188495.5592/32;            // for X = 2
//float  velocfactor2    = 94247.77961;            // for X = 1

float factorwm[5]    = {VELOCFACTORX1, VELOCFACTORX2, VELOCFACTORX4, VELOCFACTORX8, VELOCFACTORX16};
Uint16 uvdiv[5]        = {UPEVENTDIV1, UPEVENTDIV2, UPEVENTDIV4, UPEVENTDIV8, UPEVENTDIV16};

// PASAN A SER GLOBALES MRA 02-2023
//float  veloc_radsec;
float  veloc_rpm;
float  veloc_hz;

float  veloc_radsec;
float  veloc_rpm;
float  veloc_hz;

// float  hvfactor = 9.42478;  // for EQEP_TIME_OUT_PERIOD=10000L
// float  hvfactor = 6.28318;  // for EQEP_TIME_OUT_PERIOD=15000L
float  hvfactor = 4.71238;  // for EQEP_TIME_OUT_PERIOD=20000L
// float  hvfactor = 3.1416;   // for EQEP_TIME_OUT_PERIOD=30000L
// float  hvfactor = 1.5708;   // for EQEP_TIME_OUT_PERIOD=60000L
// float  hvfactor = 1.0472;   // for EQEP_TIME_OUT_PERIOD=90000L
// float  hvfactor = 1.0472/2; // for EQEP_TIME_OUT_PERIOD=180000L

float  hvel_radsec;
float  hvel_hz;

Uint16 auxeqeptmr = 0;

// clang-format on

// EQEP Interrupt Subroutine
interrupt void PTC5Feqep_isr() {

    // Edge Capture and Unit Time Out Direction Speed
    // The position of the encoder is reverse-> forwards is actually backwards
    if (EQep1Regs.QEPSTS.bit.QDF) { // Forward Diretcion QDF=1,
        eqeptmr = 0 - (float)EQep1Regs.QCPRDLAT;
        hdx = 0 - (float)EQep1Regs.QPOSLAT;
    } else { // Reverse Direction QDF=0
        eqeptmr = (float)EQep1Regs.QCPRDLAT;
        hdx = (float)QEPCNTMAX - (float)EQep1Regs.QPOSLAT;
    }
    // Capture Module error
    if (EQep1Regs.QEPSTS.bit.COEF) {
        EQep1Regs.QEPSTS.bit.COEF = 1;
    } else {
        wm_k = (float)hdx * (float)hvfactor;
    }
    EQep1Regs.QCLR.bit.UTO = 1;      // Clears Unit Time Out Interrupt Flag
    EQep1Regs.QCLR.bit.INT = 1;      // Clears Global EQEP1 Interrupt Flag
    PieCtrlRegs.PIEACK.bit.ACK5 = 1; // Clear the PIEACK of Group 5 for enables
                                     // Interrupt Resquest at CPU Level
}

// clang-format off
void PTC5Feqep_start(){
// GPIO Configure
EALLOW;                                    // Enable writing to EALLOW protected registers
SysCtrlRegs.PCLKCR3.bit.GPIOINENCLK = 1;   // Enable the SYSCLKOUT to the GPIO
SysCtrlRegs.PCLKCR1.bit.EQEP1ENCLK  = 1;   // EQEP1 Module is Clocked by the SYSCLKOUT
GpioCtrlRegs.GPBMUX2.bit.GPIO50     = 1;   // JP3 #13 GPIO50 as EQEP1A(Input)
GpioCtrlRegs.GPBMUX2.bit.GPIO51     = 1;   // JP3 #14 GPIO51 as EQEP1B(Input)
GpioCtrlRegs.GPBMUX2.bit.GPIO53     = 1;   // JP3 #14 GPIO53 as EQEP1I(Input)
EDIS;                                      // Disable writing to EALLOW protected registers
// QDU Module Configuration
EQep1Regs.QDECCTL.bit.QSRC          = 00;  // EQEP1 as Quadrature Count Mode
EQep1Regs.QDECCTL.bit.QAP           = 0;   // EQEP1A input polarity No Effect
EQep1Regs.QDECCTL.bit.QBP           = 0;   // EQEP1B input polarity No Effect
EQep1Regs.QDECCTL.bit.QIP           = 0;   // EQEP1I input polarity No Effect
EQep1Regs.QDECCTL.bit.QSP           = 0;   // EQEP1S polarity No Effect
EQep1Regs.QDECCTL.bit.SWAP          = 0;   // Quadrature-clock inputs are not swapped
EQep1Regs.QDECCTL.bit.IGATE         = 0;   // Disable gating of index pulse
EQep1Regs.QDECCTL.bit.XCR           = 0;   // 2x Resolution Count
EQep1Regs.QDECCTL.bit.SOEN          = 0;   // Disable position-compare syn output
EQep1Regs.QDECCTL.bit.SPSEL         = 0;   // Index pin is used for sync output
// PCCU Module Configuration
EQep1Regs.QEPCTL.bit.WDE            = 0;   // Disable the EQEP watchdog timer
EQep1Regs.QEPCTL.bit.QCLM           = 1;   // EQEP capture latch on Unit Time Out
EQep1Regs.QEPCTL.bit.QPEN           = 1;   // Enable EQEP position counter
EQep1Regs.QEPCTL.bit.PCRM           = 3;   // Position Counter Reset on Unit Time Event
EQep1Regs.QEPCTL.bit.SEI            = 0;   // Strobe Event actions disable
EQep1Regs.QEPCTL.bit.IEI            = 0;   // Index Event actions disable
EQep1Regs.QEPCTL.bit.SWI            = 0;   // Software Initialization action enable
EQep1Regs.QEPCTL.bit.IEL            = 0;   // Index Event Latch Reserved
EQep1Regs.QEPCTL.bit.SWI            = 0;   // Enable Software initialization
EQep1Regs.QEPCTL.bit.FREE_SOFT      = 0x10;// Position Counter is Unaffected by emulation suspend
EQep1Regs.QPOSINIT                  = 0;   // EQEP Counter Initial Position
EQep1Regs.QPOSMAX                   = QEPCNTMAX-1;// EQEP Counter Max Position
EQep1Regs.QPOSCMP                   = QEPCNTMAX;// EQEP Position Compare
EQep1Regs.QCTMR                     = 0;   // EQEP Position Compare
// Position-Compare Configuration
EQep1Regs.QPOSCTL.bit.PCSHDW        = 0;   // EQEP Position-Compare Load immediate
EQep1Regs.QPOSCTL.bit.PCLOAD        = 0;   // Position Compare Loads in QPOSCNT=0
EQep1Regs.QPOSCTL.bit.PCPOL         = 0;   // Polarity of sync output Active High pulse output
EQep1Regs.QPOSCTL.bit.PCE           = 0;   // Position Compare Disable
// Edge Capture Unit Configuration w_measure = velocfactor * X / dT
EQep1Regs.QCAPCTL.bit.UPPS          = UPEVENTDIV1;// EQEP Unit Event /1 (X=1)
EQep1Regs.QCAPCTL.bit.CCPS          = SYSCLKDIV32;// EQEP Capture Timer Prescaler /32
EQep1Regs.QCAPCTL.bit.CEN           = 1;   // EQEP Capture is Enable
// UTIME Configuration
EQep1Regs.QUPRD                     = EQEP_TIME_OUT_PERIOD;// Unit Time Out Period
EQep1Regs.QEPCTL.bit.UTE            = 1;   // Enable the EQEP Unit Timer
// Interrupt Configuration
EALLOW;                                    // This is needed to write to EALLOW protected registers
PieVectTable.EQEP1_INT              = &PTC5Feqep_isr;// EQEP Interrupt Address
EDIS;                                      // Disable writing to EALLOW protected registers
IER |= M_INT5;                             // Enable EQEP1 CPU-PIEIER5 for INT5 (Group 5)
PieCtrlRegs.PIEIER5.bit.INTx1       = 1;   // Enable the EQEP1_INT PIEIER5.1 to interrupt request sent to CPU Level
PieCtrlRegs.PIEACK.bit.ACK5         = 1;   // Clear the PIEACK of Group 5 for enables Interrupt request at CPU Level
EQep1Regs.QEINT.bit.UTO             = 1;   // Unit Time Out Interrupt Enable
}
