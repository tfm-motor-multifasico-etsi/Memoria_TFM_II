\chapter{Estado inicial}\label{ch_4}

La velocidad del motor es una medida crucial para el control. Para muchos
controles es directamente el objetivo de control y en muchos otros se trata de
una magnitud clave para estimar otras medidas. El rizado en esta magnitud
afecta a todo el control. Si hubiese artefactos, como inyección de ruido en la
medida; podrían llegar a hacerse inestables los controles. Es de vital
importancia que esta medida sea tan fidedigna como sea posible \autocite{novel}.

Hasta el momento, se ha tomado esta medida utilizando una interrupción
temporizada por el módulo ``Unit Timer Base'' y utilizando los valores de
configuración que se pueden ver en el \autoref{lst:ch_4:config}. En él se
configura el módulo EQEP para que haya una interrupción cada vez que el reloj
del sistema llegue a \tt{EQEP\_TIME\_OUT\_PERIOD}. Este valor se define
anteriormente a $20000$, que considerando la frecuencia del reloj del sistema
se corresponde con $\frac{20000}{150000000} = 133.\bar{3}~\si{\micro\second}$.

Otros datos importantes de la configuración son:
\begin{itemize}\label{item:ch_4:config}
	\item Se habilita la captura completa de cuadratura.
	\item Se habilita el módulo de captura de flancos.
	\item Se deshabilita el módulo de comparación de lectura.
	\item Tan solo se habilita la interrupción por fin del temporizador
	\item \tt{QEPCNTMAX} es $10000$ que se corresponde con las $2500$
	      ventanas por los $4$ flancos por ventana.
\end{itemize}

\lstinputlisting[language=c, caption=Configuración del EQEP.,
	label=lst:ch_4:config, firstline=123,
lastline=176]{ch_4/figures/PTC5F_eqep1.c}

Durante la interrupción se ejecutaba la función del \autoref{lst:ch_4:isr}.

\lstinputlisting[language=c, caption=Rutina de interrupción.,
	label=lst:ch_4:isr, firstline=98, lastline=120]{ch_4/figures/PTC5F_eqep1.c}

En esta función se adquiere la variación de posición del encoder en la
variable \tt{hdx}. Para ello se emplea el contador de la variable
\tt{EQep1Regs.QPOSLAT}, que se reinicia tras cada interrupción. En este
punto se invierte el signo de la velocidad. Es negativa según avanza el contador
del encoder y positivo cuando va en dirección contraria. Esto es importante
porque afecta a la secuencia de disparos en el control. En el \autoref{ch_6} se
verá como este detalle puede tener consecuencias mayores.

Después, se comprueba si ha habido algún error en el encoder. En caso contrario
se actualiza la variable global \tt{wm\_k}. Para convertir el incremento de
posición en un tiempo fijo en una velocidad en radianes por segundo hacen falta
dos cosas:
\begin{enumerate}\label{enum:ch_4:reqs}
	\item Transformar el incremento del contador a radianes.
	\item Transformar el tiempo en segundos.
\end{enumerate}
Para lo primero se puede usar el factor de conversión siguiente:
\begin{equation}\label{eq:ch_4:tick2rad}
	\frac{2\pi~\si{\radian}}{2500~\si{ventanas}\cdot4~\si{ticks\per ventana}} =
	\frac{\pi~\si{\radian}}{5000~\si{tick}}
\end{equation}
Lo segundo es cuánto se tarda, a la frecuencia del reloj del sistema, en llegar al
tiempo que se haya configurado entre interrupciones, es decir:
\begin{equation}\label{eq:ch_4:tick2sec}
	\tt{EQEP\_TIME\_OUT\_PERIOD} \cdot \tt{PERIODO\_SYSCLKOUT}=
	\frac{\tt{EQEP\_TIME\_OUT\_PERIOD}}{\tt{FRECUENCIA\_SYSCLKOUT}}
\end{equation}

Como la velocidad cumple la \autoref{eq:ch_4:pos-vel}, y el tiempo entre
interrupciones $\Delta T$ es fijo, se puede pasar con un único factor entre
intervalos de posición y velocidad.
\begin{equation}\label{eq:ch_4:pos-vel}
	v_{rad/s} = \frac{\Delta X}{\Delta T}
\end{equation}

Ese factor es \tt{hvfactor}, definido como variable
global, y basado en el cálculo de la \autoref{eq:ch_4:hvfactor}.
\begin{equation}\label{eq:ch_4:hvfactor}
	\tt{hvfactor} =
	\frac{\frac{\pi}{5000}}
	{\frac{\tt{EQEP\_TIME\_OUT\_PERIOD}}{\tt{FRECUENCIA\_SYSCLKOUT}}} =
	\frac{\pi \cdot \tt{FRECUENCIA\_SYSCLKOUT}}{5000 \cdot
		\tt{EQEP\_TIME\_OUT\_PERIOD}}
\end{equation}


El factor \tt{hvfactor} se ha precalculado y guardado de la forma que
se ver en el \autoref{lst:ch_4:hvfactor}.

\lstinputlisting[language=c, caption=Elección de hvfactor.,
	label=lst:ch_4:hvfactor, firstline=82,
lastline=88]{ch_4/figures/PTC5F_eqep1.c}

Aunque esta forma de calcular \tt{hvfactor} es muy rápida en tiempo de
ejecución, y ocupa muy poca memoria y no requiere esfuerzo en la compilación,
se puede intercambiar un poco de esfuerzo de compilación para el valor como
una constante. De esta forma, el programador no necesita acordarse de modificar
el parámetro \tt{hvfactor} si modifica el parámetro
\tt{EQEP\_TIME\_OUT\_PERIOD}. Además, se pueden utilizar valores de tiempo
entre interrupciones no precalculados y es menos opaco que un ``número mágico''.

\begin{lstlisting}[language=c, caption=Cálculo de \tt{hvfactor}
mejorado., label=lst:ch2:hvfactor_mejorado]
static const float HVFACTOR =
    3.141592 * FRECUENCIA_SYSCLOCKOUT
    / (5000.0 * (float)EQEP_TIME_OUT_PERIOD);
\end{lstlisting}

Finalmente, se escribe en los registros necesarios para que se puedan seguir
atendiendo interrupciones en el futuro.

\section{Análisis de sensibilidad}\label{sec:ch_4:Análisis_de_sensibilidad}

Para conocer la bondad de nuestra medida de la velocidad es necesario saber
como afecta el parámetro \tt{EQEP\_TIME\_OUT\_PERIOD} a las medidas de las
velocidades, ya que es el único parámetro libre. Para ello se ha realizado un
análisis de sensibilidad para le que se han realizado $15$ experimentos con
distintos valores al parámetro a distintas velocidades.

Las velocidades de los experimentos se han escogido como porcentajes de la
velocidad nominal del motor. Se han anotado los porcentajes sus
correspondientes valores de velocidad en radianes por segundo en la
\autoref{tab:ch_4:vels}.
\begin{table}[ht]
	\centering
	\begin{tabular}{c c}
		\% de $n_{sinc}$ & $\omega [\rps]$ \\
		\midrule
		$20\%$           & $20.94395$      \\
		$50\%$           & $52.35987$      \\
		$75\%$           & $78.53981$      \\
	\end{tabular}
	\caption{
		Velocidades de referencia como porcentajes de la velocidad nominal.
	}\label{tab:ch_4:vels}
\end{table}

Se han empleado los siguientes valores de \tt{EQEP\_TIME\_OUT\_PERIOD}:
$20000, 30000, 60000, 90000$ y $180000$. El periodo entre interrupciones en
y sus correspondientes valores de \tt{hvfactor} se reflejan en la
\autoref{tab:ch_4:eqep_table}.

\begin{table}[ht]
	\centering
	\begin{tabular}{c c c}
		\tt{EQEP\_TIME\_OUT\_PERIOD} & $T [\si{\milli\second}]$ & \tt{hvfactor}
\\
		\midrule
		$20000$                      & $0.1\bar{3}$             & $0.00471$
\\
		$30000$                      & $0.2$                    & $0.00314$
\\
		$60000$                      & $0.4$                    & $0.00157$
\\
		$90000$                      & $0.6$                    & $0.00104$
\\
		$180000$                     & $1.2$                    & $0.00052$
\\
	\end{tabular}
	\caption{
		Valores dependientes de \tt{EQEP\_TIME\_OUT\_PERIOD}
	}\label{tab:ch_4:eqep_table}
\end{table}

A partir de los valores de velocidad y del parámetro \tt{hvfactor} se puede
calcular el número de pulsos esperados para cada uno de los tiempos entre
interrupciones y velocidades, según la \autoref{eq:ch_4:pulsos_esperados}. El
resultado se puede ver en la \autoref{tab:ch_4:pulsos_esperados}. El número de
pulsos que se cuentan en un determinado periodo tiene que ser un número entero,
por lo que se puede deducir a priori que los que tienen números decimales
deberán tener en el régimen permanente una variación entre varios valores, que
en media tengan el valor esperado. Esto puede causar una impresión de ruido en
la medida, ya que no parará de cambiar entre los distintos posibles valores con
distancia múltiplo de \tt{hvfactor} entre sí.

\begin{equation}\label{eq:ch_4:pulsos_esperados}
	p = \frac{\omega}{\tt{hvfactor}}
\end{equation}.

\begin{table}[ht]
	\centering
	\begin{tabular}{c| c c c c c}
		$\omega$\textbackslash$T$ & $20000$      & $30000$      & $60000$      &
$90000$ & $180000$ \\
		\midrule
		$20\%$                    & $4.\bar{4}$  & $6.\bar{6}$  & $13.\bar{3}$ &
$20$    & $40$     \\
		$50\%$                    & $11.\bar1$   & $16.\bar{6}$ & $33.\bar{3}$ &
$50$    & $100$    \\
		$75\%$                    & $16.\bar{6}$ & $25$         & $50$         &
$75$    & $150$    \\
	\end{tabular}
	\caption{
		Pulsos esperados en función de la velocidad y \tt{hvfactor}
	}\label{tab:ch_4:pulsos_esperados}
\end{table}


\subsection{Resultados y análisis}

Se ha realizado una serie de experimentos basados en el código expuesto en la
\autoref{ap_1:sec:dsp_code}. Para cada experimento se ha creado un
directorio, en el que se almacena el código distintivo y los datos que se han
obtenido del DSP.\@ El análisis de datos expuesto se ha llevado a cabo con el
código
de la \autoref{ap_1:sec:analisis}.

Se han calculado las velocidades medias, que se pueden ver en la
\autoref{tab:ch_4:mean}. Estas velocidades medias están consistentemente por
encima de los valores de referencia, aunque la diferencia es de centésimas de
radián por segundo.

\begin{table}[ht]
	\centering
	\begin{tabular}{c | c c c c c}
		$\omega$\textbackslash$T$ & $20000$   & $30000$   & $60000$   & $90000$
 & $180000$  \\
		\midrule
		$0.2$                     & $21.0141$ & $20.9911$ & $20.9743$ &
$20.9947$ & $20.9272$ \\
		$0.5$                     & $52.4341$ & $52.4287$ & $52.3862$ &
$52.4654$ & $52.3955$ \\
		$0.75$                    & $78.7313$ & $78.6173$ & $78.6148$ &
$78.5762$ & $78.6340$ \\
	\end{tabular}
	\caption{
		Velocidad media $\mu(\omega) [\rps]$.
	}\label{tab:ch_4:mean}
\end{table}


Se han obtenido los perfiles de velocidad que se pueden ver en la
\autoref{fig:ch_4:resp.eps}.
Las velocidades de la \autoref{fig:ch_4:resp.eps} parecen tener algún ruido de
origen digital, especialmente en el caso en el que
\tt{EQEP\_TIME\_OUT\_PERIOD} es $20000$. Esto se alinea con la teoría
expuesta anteriormente.  Al no poder calcular la velocidad exacta a partir de
contar un número discreto de flancos en un periodo fijo, las velocidades que no
sean múltiplos exactos del número de flancos siempre oscilaran entre dos
valores que, en media, sí darán la velocidad exacta. En la
\autoref{tab:ch_4:tick_round}, se pueden ver el máximo y mínimo número de
flancos detectados durante el tiempo entre interrupciones
Este fenómeno desaparece según se aumenta el periodo de tiempo entre
interrupciones, al importar menos la contribución de un flanco
individual. A partir de cierta longitud del periodo afectan más las variaciones
reales de la velocidad del motor que la contribución de un flanco individual.

\begin{table}[ht]
	\centering
	\begin{tabular}{c|lr|lr|lr|lr|lr}
		\multirow{2}{*}{$\omega$\textbackslash$T$} &
		\multicolumn{2}{c|}{$20000$}               &
		\multicolumn{2}{c|}{$30000$}               &
		\multicolumn{2}{c|}{$60000$}               &
		\multicolumn{2}{c|}{$90000$}               &
		\multicolumn{2}{c}{$180000$}
                                          \\
		                                           & min  & max  & min  & max  &
min  & max  & min  & max  & min   & max   \\
		\midrule
		$0.2$                                      & $4$  & $5$  & $6$  & $7$  &
$13$ & $14$ & $19$ & $21$ & $39$  & $41$
		\\
		$0.5$                                      & $10$ & $12$ & $16$ & $18$ &
$33$ & $34$ & $49$ & $51$ & $98$  & $101$
		\\
		$0.75$                                     & $16$ & $17$ & $24$ & $26$ &
$49$ & $51$ & $74$ & $77$ & $148$ & $153$
		\\
	\end{tabular}
	\caption{
		Flancos redondeados
	}\label{tab:ch_4:tick_round}
\end{table}



En la \autoref{fig:ch_4:hysto.eps} se puede observar la distribución de
velocidades calculadas para distintos periodos y velocidades de referencia. Se
puede ver que, a mayor periodo y velocidad, la distribución se parece más a
una distribución normal. Es importante estudiar la desviación típica en la
medida, ya que a menor desviación mejor es la medida.

En la \autoref{tab:ch_4:var} se pueden ver las desviaciones típicas para cada
periodo entre interrupciones y velocidad. En esta tabla se puede ver que
claramente disminuye cuanto mayor es el periodo entre interrupciones. Esto es
intuitivo, por el fenómeno que ya se ha explicado anteriormente con respecto a
los flancos discretos.

Por otra parte, la desviación típica no guarda relación con la velocidad,
manteniéndose bastante constante a lo largo de las velocidades para un mismo
tiempo entre interrupciones. Esto implica que a menores velocidades la
desviación típica es proporcionalmente mayor que a grandes velocidades. Es
decir, el error porcentual cometido es mucho mayor a pequeñas velocidades,
mientras que el error absoluto permanente constante. Si dividimos la desviación
típica entre la media, podemos ver que la desviación típica es proporcionalmente
menor a mayor velocidad. El resultado se puede ver en la
\autoref{tab:ch_4:var_on_mean}.

\begin{table}[ht]
	\centering
	\begin{tabular}{c | c c c c c}
		$\omega$\textbackslash$T$ & $20000$  & $30000$  & $60000$  & $90000$  &
$180000$ \\
		\midrule
		$0.2$                     & $2.3504$ & $1.4633$ & $0.7520$ & $0.4576$ &
$0.2456$ \\
		$0.5$                     & $1.5810$ & $1.4535$ & $0.7443$ & $0.4670$ &
$0.3334$ \\
		$0.75$                    & $2.1337$ & $0.7784$ & $0.6116$ & $0.4421$ &
$0.4146$ \\
	\end{tabular}
	\caption{
		Desviaciones típicas de la velocidad $\sigma(\omega)$.
	}\label{tab:ch_4:var}
\end{table}
\begin{table}[ht]
	\centering
	\begin{tabular}{c | c c c c c}
		$\omega$\textbackslash$T$ & $20000$  & $30000$  & $60000$  & $90000$  &
$180000$ \\
		\midrule
		$0.2$                     & $0.1118$ & $0.0697$ & $0.0359$ & $0.0218$ &
$0.0117$ \\
		$0.5$                     & $0.0302$ & $0.0277$ & $0.0142$ & $0.0089$ &
$0.0064$ \\
		$0.75$                    & $0.0271$ & $0.0099$ & $0.0078$ & $0.0056$ &
$0.0053$ \\
	\end{tabular}
	\caption{
		Desviaciones típicas normalizadas por la media
		$\frac{\sigma(\omega)}{\mu(\omega)}$.
	}\label{tab:ch_4:var_on_mean}
\end{table}

Con estos resultados se podría pensar que la solución para obtener buenas
medidas es simplemente hacer el tiempo entre interrupciones lo más grande
posible. Pero esto tiene una contrapartida, es necesario controlar la máquina,
por lo que deberíamos de poder medir la velocidad con al menos el doble de
velocidad del fenómeno más rápido que se quiera poder medir, y realmente al
menos cinco veces más rápido que eso.

En todo caso, con la mejora propuesta al principio basta con escoger el tiempo
entre interrupciones y ya se calcularía la velocidad apropiadamente. Si se
conoce el tiempo característico del experimento que en el que se quiere poder
medir, basta con escoger un \\\tt{EQEP\_TIME\_OUT\_PERIOD} adecuado para
poder realizar el experimento. Pero, ¿y si se pudiera escoger
\tt{EQEP\_TIME\_OUT\_PERIOD}
dinámicamente y adaptarlo a la situación del motor?

\begin{landscape}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.8\pageheight, height=0.7\pagewidth]{
			ch_4/figures/resp.eps
		}
		\caption{
			Velocidades del eje para distintos valores de
			$\omega_{ref}$ y \tt{EQEP\_TIME\_OUT\_PERIOD}.
		}\label{fig:ch_4:resp.eps}
	\end{figure}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.8\pageheight, height=0.7\pagewidth]{
			ch_4/figures/hysto.eps
		}
		\caption{
			Distribución de la velocidades en función de
			$\omega_{ref}$ y \tt{EQEP\_TIME\_OUT\_PERIOD}.
		}\label{fig:ch_4:hysto.eps}
	\end{figure}
\end{landscape}


\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
