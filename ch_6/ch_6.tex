\chapter{Método en el bucle de control}\label{ch_6}

En este capítulo se va a presentar un método en concreto para el procesamiento
de la medida de la velocidad, pero el mayor logro de lo que se va a presentar a
continuación consiste en eliminar el uso de rutinas de interrupción del módulo
EQEP.\@ Las interrupciones deben ser cortas y evitar usar funciones complejas.
Esto limita mucho la capacidad de hacer un procesamiento de la señal avanzado,
como puede ser un filtro de Kalman, un filtro gaussiano o el modelo predictivo
propuesto en \autocite{Anuchin}.

\section{Configuración}\label{sec:ch_6:Configuración}

Para poder prescindir de las rutinas de interrupción hace falta configurar el
EQEP de una nueva forma. Lo principal es que no se habilitan las interrupciones
en absoluto y se deshabilita el módulo de temporización ``Unit Timer Base''.

Otros datos importantes son que:

\begin{itemize}\label{item:ch_6:config}
	\item Se habilita la captura completa de cuadratura.
	\item Se habilita el módulo de captura de flancos
	\item Se deshabilita el módulo de comparación de lectura.
\end{itemize}

\lstinputlisting[language=c, caption={Configuración del EQEP.},
	label=lst:ch_6:config, firstline=30,
	lastline=73]{ch_6/figures/PTC5F_eqep1.c}

\section{Algoritmo}\label{sec:ch_6:Algoritmo}

Para procesar la señal se ha empleado un algoritmo muy sencillo que se describe
en el \autoref{lst:ch_6:main}. Consiste en tomar el valor de la posición,
compararlo con el anterior, tener en cuenta si ha cruzado por la posición cero y
finalmente realizar un filtrado de la señal. Las medias móviles, y los filtros
FIR empleados en \autocite{novel, discrete_estim, error} actúan de forma
similar a un filtro de paso bajo. Por ello se ha considerado al filtrado como un
primer candidato adecuado para mostrar la capacidad de los métodos basados en
la reconciliación de datos.

\begin{lstlisting}[language=c, label=lst:ch_6:main, caption={Algoritmo de
procesamiento de medidas.}]
posCnt = EQep1Regs.QPOSCNT;
Delta_posCnt = (int32)posCnt - (int32)posCnt_ant;
if (Delta_posCnt < -QEPCNTMAX / 2) {
    Delta_posCnt += QEPCNTMAX;
} else if (Delta_posCnt > QEPCNTMAX / 2) {
    Delta_posCnt -= QEPCNTMAX;
}
wm_k = 0.99 * wm_k + 0.01 * ((float)(-Delta_posCnt) * tick_to_radps);
/*Zona de código omitida*/
posCnt_ant = posCnt;
\end{lstlisting}

\tt{EQep1Regs.QPOSCNT} es un valor entero sin signo de $32$ bits, pero puede
ser tratado como un entero de con signo de treinta y dos bits, ya que siempre
se reiniciará al llegar al valor de $10000$, el máximo número de impulsos.

Otro detalle importante es la conversión de ticks a radianes, que se hace
durante el filtrado a través de la constante \tt{tick\_to\_radps}. Esa
constante se calcula de forma muy parecida a como se calcula \tt{hvfactor} en
el \autoref{ch_4}, salvando la diferencia de que en este caso la frecuencia de
muestreo es conocida directamente y no es necesario calcularla, es
$20~\si{\kilo\hertz}$. En el \autoref{lst:ch_6:conv} se puede ver como se
calcula esa constante en el programa.

\begin{lstlisting}[language=c, label=lst:ch_6:conv, caption={Constante
de conversión.}]
const float tick_to_radps = (FRECUENCIA_MUESTREO_HZ * 2.0 * 3.141592)/(float) (QEPCNTMAX);
\end{lstlisting}

El filtro sigue la \autoref{eq:ch_6:filtro1} donde $y_k$ es la
velocidad del motor filtrada y $u_k$ la velocidad medida del motor,

\begin{equation}\label{eq:ch_6:filtro1}
	y_k = Ay_{k-1} + Bu_k
\end{equation}

que se traduce en una ecuación en el dominio de $z$ como la
\autoref{eq:ch_6:filtroz}.

\begin{equation}\label{eq:ch_6:filtroz}
	y =  Az^{-1}y + Bu
\end{equation}

Despejando $\frac{y}{u}$ se obtiene la función de transferencia de la
\autoref{eq:ch_6:filtrotr}.

\begin{equation}\label{eq:ch_6:filtrotr}
	\frac{y}{u} = \frac{Bz}{z - A}
\end{equation}

En este caso en particular, en el que $A = 0.99$  y $B=0.01$, esta función de
transferencia se puede aproximar por la de la \autoref{eq:ch_6:filtrotrs} en el
dominio de $s$. Para obtener esta función se ha utilizado la aproximación
``escalones hacia delante'' que implica $z \approx Ts - 1$.

\begin{equation}\label{eq:ch_6:filtrotrs}
	\frac{y}{u} \approx \frac{B s + \frac{B}{T}}{s + \frac{1-A}{T}} =
	\frac{0.01s+200}{s + 200}
\end{equation}

Se puede ver que tiene un polo y un cero que en este caso son estables y que es
un filtro de paso de baja. El cero actúa en la frecuencia de
$3200~\si{\hertz}$, mientras que el polo tiene una frecuencia de corte de
$32~\si{\hertz}$. Como el polo está dos décadas antes del cero, el valor de la
ganancia a frecuencias mayores de $3200~\si{\hertz}$ es $-40$ y la fase es $0$.
Por otra parte, la ganancia del filtro es $1$ en régimen permanente, por lo que
no es necesario una adaptación de señal posterior. Todo esto queda especialmente
bien ilustrado por el diagrama de Bode de la función de transferencia, que se
puede ver en la \autoref{fig:ch_6:bode}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_6/figures/bode
	}
	\caption{
		Diagrama de Bode de la función de transferencia del filtro.
	}\label{fig:ch_6:bode}
\end{figure}

Una forma útil de conceptualizar el filtro es considerar que es una ponderación
de lo que una medida puede llegar a afectar a la estimación de la velocidad.
Desde ese punto de vista, los parámetros $A$ y $B$ deben sumar $1$. Cuanto
menor sea $B$, menos capacidad tiene una medida nueva de cambiar la estimación
de la velocidad.

Utilizando la aproximación de la \autoref{eq:ch_6:filtrotrs} y considerando que
$A + B = 1$, se obtiene la \autoref{eq:ch_6:aprox}, con la que se puede
calcular la frecuencia de corte para una $B$ dada como
$\omega_c\left[\si{\hertz} \right] = \frac{B}{2\pi T}$.

\begin{equation}\label{eq:ch_6:aprox}
	\frac{y}{u} = \frac{Bs + \frac{B}{T}}{s + \frac{B}{T}}
\end{equation}

\section{
  Importancia del signo de la velocidad
 }\label{sec:ch_6:Importancia_del_signo_de_la_velocidad}

Un detalle de la línea 8 del \autoref{lst:ch_6:main} es que se invierte el
signo de \tt{Delta\_posCnt}. El sentido positivo de la velocidad es considerado
por el control como el inverso al que el EQEP considera que es el positivo. El
control toma el sentido en cuenta a la hora de llevar a cabo la modulación de
los convertidores, por lo que es fundamental que la medida de la velocidad tome
el mismo sentido como positivo.

En un principio esto no se tomó en consideración, por lo que el control no
conseguía arrancar el motor correctamente, como se puede ver en la
\autoref{fig:ch_6:malos}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_6/figures/malos
	}
	\caption{
		Velocidad a partir de cálculo erróneo.
	}\label{fig:ch_6:malos}
\end{figure}


Primero se sospechó de la implementación del algoritmo. Por lo que se simuló el
algoritmo contra unas medidas también simuladas. El cuaderno de Jupyter en el
que se llevó a cabo la simulación está adjunto en el \autoref{ap_5}. En esta
simulación se tuvo en cuenta todos los efectos mencionados en \autocite{novel}
para poder simular de forma realista un tren de medidas.

Debido a que una pregunta de uno de los foros de soporte implicaba que la
latencia del bucle aumentaba mucho al leer la variable \tt{EQep1Regs.QPOSCNT}
(\url{https://e2e.ti.com/support/processors-group/processors/f/processors-forum/
	336603/troubles-reading-qposcnt-register}), se consideró la posibilidad de
que hubiera problemas con el periodo de tiempo que era necesario para que el
bucle se ejecutase. Para comprobar si ese era el caso se conmutó un pin digital
en cada iteración del bucle y otro se puso a uno mientras que se calculaba el
control.

El resultado es el que se puede observar en la \autoref{subfig:ch_6:tek0}. En
esta figura se puede apreciar que el tiempo empleado en el bucle de control es
significativamente menor que la iteración completa del bucle. Simplemente por
comparar, se realizó el mismo experimento con un cálculo de velocidad por
interrupciones. El resultado es idéntico, como se puede ver comparando la
\autoref{subfig:ch_6:tek0} y la \autoref{subfig:ch_6:tek1}.

Los distintos picos que se pueden observar en las dos figuras se deben a que las
conexiones que se hicieron no eran muy estables ni seguras. Hubo dos factores
que influyeron negativamente a la toma de medidas. No hay ninguna superficie
plana lo suficientemente grande como para apoyar el osciloscopio de forma
segura en la que también haya acceso a los pines digitales del DSP.\@ Y no
hay muchas tierras accesibles para los conectores de las sondas digitales. Para
estos experimentos se utilizó la tierra de una alimentación de cinco voltios
que tiene el DSP, accesible desde un bornero, pero no se pudo atornillar
correctamente el punto de tierra.

\begin{figure}[ht]
	\begin{subfigure}[t]{\linewidth}
		\includegraphics[width=0.9\textwidth]{
			ch_6/figures/tek00000
		}
		\caption{
			Leyendo QPOSCNT en el bucle
		}\label{subfig:ch_6:tek0}
	\end{subfigure}
	\begin{subfigure}[b]{\linewidth}
		\includegraphics[width=0.9\textwidth]{
			ch_6/figures/tek00001
		}
		\caption{
			Utilizando interrupciones
		}\label{subfig:ch_6:tek1}
	\end{subfigure}
	\caption{
		Temporización de eventos del bucle de control
	}\label{fig:ch_6:temp_events}
\end{figure}
\FloatBarrier{}

\section{Experimentos}\label{sec:ch_6:Experimentos}

El primer experimento que se ha realizado es dejar que el motor alcance el
régimen permanente a distintas velocidades. El resultado se puede ver en la
\autoref{fig:ch_6:Reg_perm2}. Se puede ver que el resultado es mucho más fluido
que en todos los algoritmos anteriores.

Las variaciones en la velocidad tienen una forma más suave que en los distintos
métodos anteriores. En ellos también se podían observar estas variaciones, como
eventos discretos en los que cambiaba la media durante el periodo. Al filtrar
la señal, se hace una reconstrucción de la misma, aunque si el filtro tiene una
frecuencia de corte demasiado baja, puede dar lugar a reconstrucciones
inexactas.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_6/figures/Reg_perm2
	}
	\caption{
		Distintas velocidades en régimen permanente.
	}\label{fig:ch_6:Reg_perm2}
\end{figure}

A continuación se llevaron a cabo distintos cambios en la referencia de la
velocidad, para comprobar si el sistema se adaptaba adecuadamente. En la
\autoref{fig:ch_6:change_2} se puede ver que el efecto suavizador del filtro
actúa también durante el periodo transitorio y que el sistema es estable tanto a
velocidades positivas como negativas. Es muy interesante comparar esta figura
con la \autoref{fig:ch_5:change_2}. Las características principales de la señal
como el tiempo de establecimiento y la sobreoscilación no se ven afectadas,
pero esta segunda figura es mucho más suave que la del capítulo anterior,
especialmente fuera de la zona de régimen permanente.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_6/figures/change_2
	}
	\caption{
		Cambios en la referencia de velocidad.
	}\label{fig:ch_6:change_2}
\end{figure}


Finalmente, se ha hecho una prueba más, en el que se ha aumentado la
frecuencia de corte del filtrado. Para ello se ha cambiado la línea ocho del
\autoref{lst:ch_6:main} por la siguiente línea.

\begin{lstlisting}[label={type:ch_6:label}, caption={Nuevo filtro}]
    wm_k = 0.8 * wm_k +  0.2 * ((float)(-Delta_posCnt) * tick_to_radps);
\end{lstlisting}

Este filtro se corresponde con la función de transferencia de la
\autoref{eq:ch_6:filtro2}, que tiene una frecuencia de corte de
$710.31~\si{\hertz}$. Como se puede ver en la \autoref{fig:ch_6:menos_filtro},
esto tiene como consecuencia que hay un nivel de ruido bastante considerable en
la señal, pero aun así el sistema es estable.

\begin{equation}\label{eq:ch_6:filtro2}
	\frac{y}{u} =  \frac{0.2 s + 4463}{s + 4463}
\end{equation}

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_6/figures/menos_filtro
	}
	\caption{
		Escalón con mayor ancho de banda en el filtro.
	}\label{fig:ch_6:menos_filtro}
\end{figure}

\FloatBarrier{}

\section{Conclusión}\label{sec:ch_6:Conclusión}

Se ha habilitado una forma especialmente potente de procesar las medidas,
realizando las medidas y la reconciliación de los datos en el bucle de
corriente. Este nuevo método abre la posibilidad de usar filtros estadísticos y
otras formas de mejorar las medidas que no era posible implementar durante una
interrupción.

Se ha probado un filtro de paso bajo sencillo, con un polo y un cero. Este
filtro reconstruye bastante bien la señal de velocidad, teniendo mejores
características que los algoritmos anteriores y sin que afecte a las
observaciones de forma dramática. No se han observado cambios en el tiempo de
establecimiento, ni en la sobreoscilación. El filtro diseñado no modifica los
comportamientos conocidos del motor, pues la mayor parte de esos
comportamientos están por debajo de la frecuencia de corte.

Se ha probado a aumentar la frecuencia de corte con el consiguiente aumento del
ruido. Como conocemos de los algoritmos anteriores de dónde procede este ruido,
de la discretización de la velocidad, sabemos que puede ser filtrado. De la
misma forma que en el algoritmo del capítulo anterior se explotó la inercia
mecánica del motor para poder realizar cambios en el tiempo entre mediciones
sin que hubiera mayores consecuencias, en este caso se puede explotar la
inercia del motor para disminuir la frecuencia de corte. En general se
recomiendan frecuencias de corte por debajo de los $100~\si{\hertz}$, pero
mayores de los $30~\si{\hertz}$.
\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
