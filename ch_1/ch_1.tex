\chapter{Introducción}\label{ch_1}

\section{Antecedentes}\label{sec:ch_1:Antecedentes}

Este trabajo tratará de mejorar las tácticas para obtener información sobre la
velocidad del motor de la bancada experimental de accionamientos multifásicos
del Departamento de Ingeniería Electrónica de la Universidad de Sevilla.

Este tipo de máquinas son más robustas ante fallos, pudiendo llegar funcionar
con fases en falta con la programación adecuada \autocite{dclink, 15fases}. Esto puede ser crucial en sistemas de soporte vital y
operaciones críticas y muy útil en localizaciones remotas o de difícil acceso
para el mantenimiento.

Estos motores requieren un menor amperaje por fase para una misma potencia, lo
que reduce la carga sobre la electrónica de potencia y los cables y permitiendo
que los motores sean más compactos para un torque dado \autocite{potrpm}. Esto
los hace de especial interés actualmente para los sistemas de transporte.

Debido a los grados de libertad adicionales que tienen estos accionamientos,
reducen el rizado del par transmitido con respecto a motores similares de tres
fases \autocite{orig_torque} o los armónicos cuando se utilizan estas máquinas
para la generación de energía \autocite{dclink}.

La velocidad de un motor es una magnitud clave para el control, muchas veces
siendo el objetivo de control \autocite{Anuchin, novel}. En consecuencia,
distintos autores han realizado bastante investigación al respecto de cómo
mejorar las medidas.

En \autocite{discrete_estim} se examina cómo afecta el periodo de muestreo a la
estimación de la velocidad. Se descubre que, para tiempos de muestreo altos, se
requieren más términos de la expansión de Taylor de la aproximación de la
derivada para mantener la exactitud. Se compara este método con un método de
mínimos cuadrados para polinomios de orden mayor que dos, con el método clásico
y con un método de media móvil combinado con los métodos anteriores. El
resultado en simulación es que cualquier método empleado individualmente es
mejorado con el método de media móvil.

En \autocite{error} se presenta un criterio para cambiar la base de cálculo
de velocidades de frecuencia a periodo, este método no es realizable en el
motor por motivos tecnológicos, como se verá en el \autoref{ch_3}. Este problema
ya se menciona en \autocite{Anuchin} para sistemas que usan microcontroladores.
También se propone el uso de una media móvil. En este artículo se menciona un
método sugerido en \autocite{improved_but_worse} que consiste en ajustar el
periodo de muestreo de forma que sea un múltiplo exacto de la velocidad para
evitar el error de cuantización. Lo más importante de este artículo es que
sugiere una forma de combinar todos los métodos. Ni en \autocite{error} ni en
\autocite{improved_but_worse} se realizan experimentos con un motor real.

En el artículo \autocite{novel}, se presenta un modelo para el ruido de
cuantización introducido por el encoder, considerando que no es un ruido
blanco, sino que está agrupado en torno a unas frecuencias. Para eliminar el
ruido cuando el motor funciona a velocidad constante se puede calcular un
filtro de tipo Notch en función de la velocidad angular y del tiempo de
muestreo. También se incluye un método para desplazar el ruido en frecuencia, de
forma que se pueda utilizar el filtro Notch de forma más efectiva. Los
resultados son buenos, aunque se observa que un filtro FIR funciona mejor para
reducir el ruido en general de la velocidad durante los periodos transitorios y
tienen una FFT más limpia. Sin embargo, el modelo propuesto es el más realista
que se ha encontrado en la literatura y todos los fenómenos mencionados en el
artículo se han observado en la práctica durante la ejecución del trabajo.

En \autocite{Anuchin} se crea un modelo predictivo con ventana variable de la
velocidad para estimar la velocidad a partir de medición. Este método funciona
muy bien durante los transitorios, pero sufre durante los regímenes permanentes
en su versión sin ventanas variables. Para su funcionamiento óptimo las
velocidades e inercias deben ser altas. Los autores recomiendan el uso de FPGA
cuando se requiera una alta frecuencia de muestreo.


\section{Sistema}\label{sec:ch_1:Sistema}

La bancada experimental se encuentra en el laboratorio de electrónica de la
Escuela Técnica Superior de Ingeniería de la Universidad de Sevilla. Es una
bancada en la que el sistema principal es un motor de cinco fases.

Este accionamiento está conectado a una máquina de continua que sirve para
simular cargas o pares de generación. Este motor de continua está controlado por
un armario dedicado.

El motor multifásico está accionado por inversores situados en otro armario
dedicado al control de la máquina multifásica. Estos convertidores están
alimentados en su lado de continua por una fuente de continua. El control
de los convertidores se hace desde un DSP.\@

Se puede interactuar con ambos armarios una vez que estan en marcha a través de
un ordenador central, que se comunica con los distintos armarios y con la
fuente a través de comunicaciones seriales.

En el \autoref{ch_2} se describirán en mayor profundidad los distintos
elementos de la bancada. En la \autoref{fig:ch_1:Esquema_sistema} se puede ver
el diagrama de alto nivel de los flujos de información y potencia de la bancada.

\begin{figure}[ht]
	\centering
	\includegraphics[height=0.5\textheight]{
		ch_1/figures/sistema
	}
	\caption{
		Esquema del sistema utilizado \autocite{en12244698}
	}\label{fig:ch_1:Esquema_sistema}
\end{figure}

\section{Organización}\label{sec:ch_1:Organización}

En primer lugar se van a describir la bancada de forma exhaustiva en el
\autoref{ch_2}, mencionando los modelos, el estado y la información más
relevante de cada uno de los componentes.

En el \autoref{ch_3} se va a describir el funcionamiento del módulo EQEP del
DSP.\@ Se explicará someramente cómo funcionan los encoders en general y se
procederá a explicar en un alto nivel de detalle todos los submódulos relevantes
del EQEP en este trabajo.

En el \autoref{ch_4} se examinará el estado del arte en la bancada, examinando
qué puntos son susceptibles de mejora y cómo se podrían llevar a cabo dichas
mejoras.

Se continúa en el \autoref{ch_5} con posibles mejoras que evitan alterar
significativamente el flujo de trabajo hasta el momento. Los dos algoritmos
alternativos que se plantean modifican los valores de configuración del EQEP
para optimizar la velocidad de la toma de datos y el error asociado a esta.

Para contrastar, en el \autoref{ch_6} se propone un algoritmo que se basa en la
reconciliación de datos y no modifica la configuración del
módulo EQEP durante la ejecución.

Finalmente, en el \autoref{ch_7}, se compararán los distintos algoritmos en las
conclusiones.

\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
