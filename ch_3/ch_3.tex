\chapter{Descripción del módulo EQEP}\label{ch_3}

En este capítulo se explicará el funcionamiento del módulo EQEP.\@ Este capítulo
se basa sobre el capítulo 6 del manual \textcite{eqep}, habiendo partes
traducidas directamente del mismo.

El DSP dispone de un módulo llamado ``Enhanced Quadrature Encoder Pulse'', que
contiene una interfaz para encoders de cuadratura. Con este módulo se puede
calcular la posición, velocidad y dirección del encoder. Está diseñado para
ser rápido y funcionar bien en aplicaciones de control de posición y velocidad.

\section{Encoders}
Un encoder rotatorio es un sensor que es capaz de convertir posición o
movimiento en una señal. Para ello se usa un disco en el que hay una
serie de ranuras a través de las cuales puede pasar un haz de luz que incide
sobre un foto-diodo, que producen una señal eléctrica. En el disco suele haber
una posición de cero grabada en una fila aparte.

Utilizando dos sensores desplazados la distancia de media ranura, se puede
obtener información sobre la dirección y se cuadriplica la precisión del
sensor utilizando los flancos de subida y bajada de los dos sensores. En el
manual se ilustra que la silueta del disco y las señales que se producen
durante el funcionamiento en la \autoref{fig:ch_3:encoder_pulsos}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/encoder_pulsos
	}
	\caption{
		Pulsos producidos por un encoder incremental \autocite{eqep}
	}\label{fig:ch_3:encoder_pulsos}
\end{figure}

El encoder de la bancada es el modelo \tt{10-11657-2500} de Hohner. Se
puede ver una fotografía del mismo en la \autoref{fig:ch_3:DSC_0047.JPG}. Según
las especificaciones de este encoder en \autocite{encoder} es un sensor con
$2500$ ventanas por vuelta, por lo que la resolución es la que se calcula en la
\autoref{eq:ch_3:resolución_2}.

\begin{equation}\label{eq:ch_3:resolución_2}
	\frac{2500~\si{pulsos}\cdot4~\si{pulsos\per
			flanco}}{\ang{360}}=27.\bar{7}~\si{flancos \per
		\degree}
	\equiv
	\frac{10000~\si{flancos}}{2\pi~\si{\radian}}\approx1591.55~\si{flancos\per
		\radian}
\end{equation}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{
		ch_4/figures/DSC_0047.JPG
	}
	\caption{
		Encoder.
	}\label{fig:ch_3:DSC_0047.JPG}
\end{figure}

El encoder proporciona la posición, pero no la velocidad. No obstante, se puede
obtener esta última a partir de la primera. Para conocer cómo se obtiene, es
necesario analizar el funcionamiento del encoder, el módulo dedicado a la
captura de pulsos de encoders de cuadratura del DSP y como funciona el programa
que calcula la velocidad del motor.

\section{Arquitectura}\label{sec:ch_3:Arquitectura}

El módulo EQEP se divide en varios submódulos que se configuran de forma más o
menos independiente. Estos módulos tienen registros de configuración y en
muchos casos registros de salida. La \autoref{fig:ch_3:encoder_functional} es un
mapa funcional de como se relacionan los distintos submódulos.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/encoder_functional
	}
	\caption{
		Diagrama funcional del módulo EQEP \autocite{eqep}
	}\label{fig:ch_3:encoder_functional}
\end{figure}

En este capítulo no se explicará en profundidad todos los submódulos, sino en
cada submódulo los aspectos más relevantes para el resto del trabajo.

La información más relevante que se puede extraer del diagrama de la
\autoref{fig:ch_3:encoder_functional} y descripción de la arquitectura es que
\begin{itemize}\label{item:ch_3:improv_list}
	\item Las únicas entradas al sistema son los pines por los cual entra la
	      señal del encoder de cada uno de los fotodiodos, la entrada optativa de la
	      ranura de cero, la entrada optativa de cero externo y el reloj del sistema.
	\item La decodificación, el conteo, la captura y la generación de
	      interrupciones ocurren en unidades distintas.
	\item Existen registros compartidos y registros con nombres muy similares
	      entre sí. Hay que tenerlo presente para cuando se realicen las configuraciones
	      de los módulos. En concreto, los registros \tt{QCPRD}, \tt{QCTMR},
	      \tt{QUPRD}, \tt{QUTMR}, \tt{QWDPRD}, \tt{QWDTMR} son tres
	      juegos de registros que son muy importantes y es sencillo confundirlos.
	\item Que existen registros acoplados (``latched'') que tienen contenido
	      distinto de los registros sin acoplar.
	\item Que existen registros que en el diagrama que parecen ser entradas
	      pero que en realidad son de entradas y salidas a la vez. Como por ejemplo
	      \tt{QPOSCNT}, que admite lectura y escritura (aunque el
	      manual desrecomienda la escritura).
\end{itemize}

\section{Quadrature Decoder Unit}\label{sec:ch_3:Quadrature_Decoder_Unit}


El decodificador de cuadratura (QDU) convierte el tren de pulsos %TODO esta
%frase no es la adecuada
de las señales digitales de los pines en una señal de flancos y una señal de
dirección, además de otras señales.

La configuración de este módulo permite cómo se deben interpretar los flancos,
ya que algunos encoders, distintos de los de la bancada, utilizan otras formas
de codificar su señal de posición. También, permite alterar la polaridad de las
señales en el módulo o si se deben invertir.

La única información directamente accesible al usuario de este módulo es la
dirección de giro, a través de \tt{QEPSTS:QDF}, y la bandera de error en la
fase, a través de \tt{QFLG:PHE}. Este módulo actúa principalmente para
convertir la señal, dejando a los siguientes módulos la tarea del procesado de
la información.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/esquema_transiciones
	}
	\caption{
		Máquina de estados que determina el incremento de la posición \autocite{eqep}.
	}\label{fig:ch_3:esquema_transiciones}
\end{figure}

\section{Position Counter and Control Unit}\label{
	sec:ch_3:Position_Counter_and_Control_Unit}

La unidad de conteo y control de posiciones (PCCU) proporciona información
sobre la posición del encoder a partir de las señales producidas en la QDU.\@
Tiene varios modos de operación que dependen de cuando se reinicie el contador.
El contador interno se puede reiniciar cada vez que pase por el cero, cada vez
que se llegue a la máxima posición, cada vez que se termine un cronómetro o de
una forma mixta.

En el primer caso, usa la señal externa de índice para reiniciar el contador. En
el segundo, cada vez que llegue a un máximo que debe ser configurado por el
usuario del módulo. En el tercero cronómetro lo reinicia periódicamente. Esto es
especialmente útil si además el cronómetro se emplea para acoplar las medidas y
utilizarlo en una interrupción. Por último, la forma mixta consiste en encontrar
primero el índice y después usar el segundo método. Este puede ser útil
cuando por algún motivo se quiera reiniciar el contador con frecuencias
distintas a las del índice, como por ejemplo si el motor estuviera conectado a
una reductora.

En la \autoref{fig:ch_3:encoder_qpos_cont.png} se puede ver el funcionamiento
en el primero de los modos. Las señales que recibe del encoder son \tt{QA},
\tt{QB} y \tt{QI}. El módulo QDU genera \tt{QCLK} y
\tt{QEPSTS:QDF}. El módulo PCCU genera \tt{QPOSCNT}, \tt{QPOSILAT} y
\tt{QEPSTS:QDLF}. Se puede ver que el índice de interrupciones no salta en
uno de los flancos de \tt{QI}, sino que depende de la dirección de giro del
encoder.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/encoder_qpos_cont.png
	}
	\caption{
		Transiciones basadas en índice\autocite{eqep}.
	}\label{fig:ch_3:encoder_qpos_cont.png}
\end{figure}

Otro parámetro importante es cuándo se acoplan (latch) las variables. Se puede
acoplar una vez que llegue el contador a una posición, cada vez que haya un
paso por el índice o cada vez que haya una señal de Strobe (una señal digital
externa optativa para sincronizar capturas en varios módulos). En la
\autoref{fig:ch_3:encoder_qpos_cont.png} los valores se acoplan en con el
índice.

La inicialización del contador se puede levar a cabo escribiendo en un registro,
por la señal de Strobe o por el paso por el índice.


\section{Edge Capture Unit}\label{sec:ch_3:Edge_Capture_Unit}

La unidad de captura de flancos sirve para capturar valores significativos del
módulo. Puede funcionar de dos formas: recibiendo eventos externos de la unidad
de temporización, o por lectura de la CPU de la variable \tt{QPOSCNT}. Las
características más relevantes de este submódulo son la frecuencia con la que
registra un evento de captura y cuándo acopla las variables \tt{QPOSLAT},
\tt{QCPRDLAT}, y \tt{QCTMRLAT}.

Esta unidad tiene un cronómetro interno de dieciséis bits, que avanza en una
división del reloj del sistema, configurable mediante el registro
\tt{QCAPCTL:CCPS}. Esta división es la inversa de una potencia de
dos entre cero y siete. Si se configurase la división máxima, sería equivalente
a tener un cronómetro de $16 + 7 = 23$ bits. Esta unidad es la que genera los
valores de \tt{QCPRDLAT}, y \tt{QCTMRLAT}. El valor de
\tt{QCTMRLAT} es el valor del cronómetro en el momento del acoplamiento. En
\tt{QCPRDLAT} se almacena el tiempo entre los últimos eventos.

El submódulo genera un evento de forma configurable mediante el registro
\tt{QCAPCTL:UPPS}. Se puede escoger una potencia de dos para que cada $n$
pulsos de \tt{QCLK} se genere un evento. Los posibles valores de $n$ son $n
	\in \{ 2^x | x\in \mathbb{N},0\leq x \leq11\}$.

Esta unidad tiene dos modos de acoplar los valores de los contadores y
cronómetros a las variables. Una de ellas depende de las lecturas de la CPU del
valor de \tt{QPOSCNT}. La otra depende de las interrupciones del módulo de
temporización.

Cuando usa la lectura de \tt{QPOSCNT} para capturar los valores se acoplan
\tt{QCTMRLAT} y \tt{QCPRDLAT} se puede calcular la velocidad mínima de
rotación que es posible medir. La velocidad mínima es la que provoca un
desbordamiento del cronómetro entre los eventos más cercanos posibles. En la
\autoref{eq:ch_3:min_vel} se plantea este cálculo para un encoder de $N$ pulsos
y frecuencia de reloj del sistema $f_{sys}$. En el caso particular de la
bancada, con $f_{sys} = 150~\si{\mega\hertz}$ y un encoder de $N = 2500$
pulsos, la velocidad mínima es $0.028~\rps$.

\begin{equation}\label{eq:ch_3:min_vel}
	\omega_{\min}\left[\rps\right] = \frac{\frac{2\pi}{4\cdot
			N}}{\frac{2^{23}}{f_{sys}}}
\end{equation}

Cuando recibe los eventos del cronómetro, entonces guarda la posición en
\tt{QPOSLAT}, y los valores de tiempo y periodo en \tt{QCTMRLAT} y
\tt{QCPRDLAT}. La variable \tt{QPOSLAT} refleja la posición en el
momento en el que se dispara el cronómetro.

Se incluye la \autoref{fig:ch_3:esquema_capture} para aclarar qué valores se
capturan.\ \tt{QCPRDLAT} es $\Delta T$. El valor de \tt{QPOSCNT} es el
que se acopla en \tt{QPOSLAT} y el valor de \tt{QCTMR} es el que se
acopla en \tt{QCTMRLAT}.\ \tt{UPEVENT} es la señal de eventos que en
este caso está configurado cada dos pulsos de \tt{QCLK}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/esquema_capture
	}
	\caption{
		Esquema de los valores capturados por la unidad \autocite{eqep}.
	}\label{fig:ch_3:esquema_capture}
\end{figure}

En la \autoref{fig:ch_3:encoder_capture} se puede ver el diagrama funcional del
módulo, el que queda especialmente claro qué valores configuran qué. Queda por
explicar la función de varios registros y valores de configuración.
\tt{QCAPCTL:CEN} es un bit que habilita el funcionamiento del módulo.
\tt{QEPSTS:COEF} registra si ha habido un desbordamiento en los
temporizadores.\ \tt{QEPSTS:UPEVNT} si se ha contado algún pulso. Y
\tt{QEPSTS:CDEF} si ha habido algún cambio de dirección.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/encoder_capture
	}
	\caption{
		Esquema funcional de la unidad de captura\autocite{eqep}.
	}\label{fig:ch_3:encoder_capture}
\end{figure}

\section{Unit Timer Base}\label{sec:ch_3:Unit_Timer_Base}

Esta unidad se compone de un cronómetro de treinta y dos bits que utiliza el
reloj del sistema como fuente para generar interrupciones de forma regular. Para
ello dispone del registro \tt{QUPRD} que es el periodo con el que se generan
las interrupciones.

El ancho en bits permite calcular la velocidad mínima que genera al menos un
pulso durante el máximo periodo del cronómetro,  $2^{32}$ ticks del reloj del
sistema. Para el cálculo se puede ver en la \autoref{eq:ch_3:min_vel2} que es
igual en estructura a la \autoref{eq:ch_3:min_vel}. La velocidad mínima con
este método en la bancada es $21.9~\si{\micro\radian\per\second}$.

\begin{equation}\label{eq:ch_3:min_vel2}
	\omega_{\min}'\left[\rps\right] = \frac{\frac{2\pi}{4\cdot
			N}}{\frac{2^{32}}{f_{sys}}}
\end{equation}

En general, se puede calcular que método tiene mayor precisión dividiendo las
velocidades mínimas calculables, como se hace en la
\autoref{eq:ch_3:min_vel_comp}. Cuanto mayor sea el resultado, peor es la
precisión del dividendo con respecto al divisor. En este caso es $512$ veces
mayor la velocidad mínima calculada a partir del módulo descrito en la
\autoref{sec:ch_3:Edge_Capture_Unit} que la velocidad mínima calculable a
partir de las interrupciones generadas por este módulo. Por este motivo es
mejor usar el método de las interrupciones temporizadas para medir velocidades
muy bajas y no se puede usar la mejora propuesta en \autocite{error}.

\begin{equation}\label{eq:ch_3:min_vel_comp}
	\frac{\omega_{\min}}{\omega_{\min}'} = \frac{
		\frac{\frac{2\pi}{4\cdot
				N}}{\frac{2^{23}}{f_{sys}}}
	}{
		\frac{\frac{2\pi}{4\cdot
				N}}{\frac{2^{32}}{f_{sys}}}
	} =
	\frac{2^{32}}{2^{23}} = 2^{9} = 512
\end{equation}

En la \autoref{fig:ch_3:diag_bloques_time} se puede ver el diagrama de bloques
del submódulo de temporización. Este bloque es especialmente sencillo, tiene
una entrada, una salida y dos valores configurables y dos valores
internos.\ \tt{SYSCLOCKOUT} es el reloj del sistema.\ \tt{QEPCTL:UTE}
es un bit de habilitación del módulo.\ \tt{QUTMR} es el registro donde se
lleva a cabo la temporización.\ \tt{QUPRD} es el valor del temporizador en
el que se quiere que se provoque la interrupción.\ \tt{UTOUT} es una señal
en la que se produce un flanco cada vez que \tt{QUPRD} es igual a
\tt{QUTMR}.\ \tt{QFLG:UTO} es un bit en el que guarda si se ha
producido el evento. El usuario del módulo debe despejar este bit cuando llegue
a la interrupción si quiere tener información fidedigna del origen de sus
interrupciones.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_3/figures/diag_bloques_time
	}
	\caption{
		Esquema de bloques de la unidad temporizadora \autocite{eqep}.
	}\label{fig:ch_3:diag_bloques_time}
\end{figure}

\section{Interrupts}\label{sec:ch_3:Interrupts}

La forma más frecuente de utilizar el módulo es habilitando las interrupciones
y programando qué interrupciones se aceptan para el cálculo de la velocidad.
Existen once tipos de interrupciones que se puede habilitar individualmente en
el registro \tt{QEINT}. El origen de las interrupciones se refleja en el
registro \tt{QFLG}, que debe ser despejado tras atenderlas usando el
registro \tt{QCLR}. De lo contrario, no se generan más interrupciones de
ese origen.

\begin{enumerate}\label{enum:ch_3:interrupts}
	\item \tt{PCE}: Ha habido un error en el contador de posición.
	\item \tt{QPE}: Ha habido un error en la fase de la cuadratura.
	\item \tt{QDC}: Ha habido un cambio de dirección.
	\item \tt{WTO}: Ha terminado la temporización del watchdog.
	\item \tt{PCU}: Ha habido un desbordamiento inferior del contador de
	      posiciones.
	\item \tt{PCO}: Ha habido un desbordamiento superior del contador de
	      posiciones.
	\item \tt{PCR}: Se ha preparado la comparación de posiciones.
	\item \tt{PCM}: Se han comparado las posiciones con éxito.
	\item \tt{SEL}: Se ha dado el evento de acoplamiento de variables por
	      fuente externa (Strobe).
	\item \tt{IEL}: Se ha dado el evento de acoplamiento de variables por
	      índice.
	\item \tt{UTO}: Se ha llegado al fin del cronómetro del Unit Timer
	      Base.
\end{enumerate}

\section{Conclusión}\label{sec:ch_3:Conclusión}

El módulo de EQEP de Texas Instruments es muy potente para las aplicaciones que
usan un encoder. Es capaz de procesar distintos tipos de encoders, y de
funcionar en varias configuraciones distintas. También es capaz de modificar la
configuración durante la ejecución del programa, dotándolo de una alta
flexibilidad. La mayor dificultad reside precisamente en realizar una
configuración inicial adecuada. Es sencillo olvidar o configurar erróneamente
algún parámetro. Configuraciones erróneas pueden llevar a sorpresas en el
funcionamiento del encoder y en ocasiones a fallos en el programa mientras que
está en lo que parece funcionamiento normal.

En el resto del trabajo se utilizará en dos configuraciones. La primera se basa
en las interrupciones del módulo Unit Timer Base y la otra evita las
interrupciones leyendo los valores del contador en el bucle de control
principal. En los dos casos se comentará la configuración específica.

\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
