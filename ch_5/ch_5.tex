\chapter{Métodos de tiempo adaptativo}\label{ch_5}

El parámetro \tt{EQep1Regs.QUPRD} se puede cambiar durante el funcionamiento
del módulo, por lo que es posible elaborar algoritmos que adapten el valor de
ese parámetro en función de la velocidad. A continuación se expondrán dos
formas distintas de adaptar la velocidad.

Las dos formas tienen la misma configuración inicial que en el
\autoref{lst:ch_4:config} del capítulo anterior. También tienen en común que se
es necesario recalcular el parámetro \tt{hvfactor} con frecuencia, por lo
que se ha declarado la macro que se puede ver junto con su documentación en el
\autoref{lst:ch_5:macro}

\lstinputlisting[language=c, caption={Macro para el cálculo de \tt{hvfactor}.},
	label=lst:ch_5:macro, firstline=26, lastline=42]{ch_5/figures/PTC5F_eqep1.c}

También en ambos casos se declara un temporizador inicial de $20000$ pulsos del
reloj del sistema en la definición de la macro \tt{INITIAL\_TIMEOUT}.

\section{Adaptación con compromiso}\label{sec:ch_5:Adaptación_con_compromiso}

Este algoritmo está basado en un descenso por gradiente en el que se busca
minimizar el error. Se ha adaptado para evitar que el tiempo se dispare,
parando el descenso una vez que se ha llegado a una banda aceptable de error.
Otra restricción que se ha impuesto es que no se cambie el tiempo entre
interrupciones durante los periodos transitorios. De esta forma se garantiza
que no se provocan inestabilidades durante los transitorios por alterar la
forma de medir. La última restricción que se ha impuesto es que hay un
tiempo mínimo y máximo entre interrupciones.

\subsection{El algoritmo}

En este programa lo primero que se hace es calcular que si está dentro de una
banda del $30\%$ de la velocidad de referencia. Después, se calcula la
velocidad igual que en el capítulo anterior. A continuación, se calcula si se
debe cambiar el tiempo entre interrupciones. Para ello:

\begin{itemize}\label{enum:ch_5:algorythm}
	\item Se comprueba si se debe incrementar el tiempo de interrupción,
	      comprobando que no se ha alcanzado el máximo, que se encuentra en la
	      banda en la que es aceptable incrementar, y que de hecho se va a
	      incrementar el valor del tiempo entre interrupciones. En ese caso:
	      \begin{itemize}
		      \item Se elimina la bandera de haber alcanzado el tiempo mínimo,
		      \item Se corrige el tiempo multiplicando por un factor que
		            sabemos que por fuerza es mayor que uno, y que se
		            corresponde con el factor por el que se debería multiplicar
		            el tiempo para que el error de contar un pulso de más sea el
		            máximo error aceptable.
		      \item Si se ha superado el tiempo máximo, se levanta la bandera
		            correspondiente y se corrige el tiempo para que sea el
		            máximo.
		      \item Se corrige el tiempo y se recalcula \tt{hvfactor}.
	      \end{itemize}
	\item En caso contrario y si no se ha alcanzado el mínimo, se encuentra
	      en la banda correcta, si de verdad se va a reducir el tiempo entre
	      interrupciones.
	      \begin{itemize}
		      \item Se elimina la bandera de haber alcanzado el tiempo máximo.
		      \item Se corrige el tiempo multiplicando por un factor que sabemos
		            que por fuerza es menor que uno, y que se corresponde con el
		            factor por el que se debería multiplicar el tiempo para que
		            el error de contar un pulso de más sea el máximo error
		            aceptable.
		      \item Si se sugiere un tiempo menor que el tiempo mínimo, se
		            levanta la bandera correspondiente y se corrige el tiempo
		            para que sea el mínimo posible.
		      \item Se corrige el tiempo y se recalcula \tt{hvfactor}.
	      \end{itemize}
\end{itemize}

Finalmente, se limpian los registros de las interrupciones y se prepara la
siguiente interrupción.

El código de este algoritmo se puede ver en el \autoref{lst:ch_5:adapt_1}.

\lstinputlisting[language=c, caption=Interrupción en el algoritmo adaptativo
	con compromiso, label=lst:ch_5:adapt_1, firstline=49,lastline=109]{
	ch_5/figures/PTC5F_eqep1.c}


\subsection{Experimentos}

Se han realizado varios experimentos con este algoritmo, para comprobar que
funciona correctamente. El primer experimento ha sido ver el funcionamiento
para distintas bandas de error aceptable a distintas velocidades. El resultado
se puede ver en la \autoref{fig:ch_5:Reg_perm}.

Se verifica que a mayor error aceptable mayores son los errores que se
cometen, comparando el error aceptable en los casos con $1\%$ de error
aceptable y de $5\%$ de error aceptable. También se observa que no hay una
gran diferencia entre un margen superior de error aceptable de $0.5\%$ y $1\%$,
ya que se alcanza el mayor tiempo posible en ambos casos. Finalmente se puede
ver que el tiempo entre interrupciones aumenta según disminuye la velocidad para
un mismo margen de error. Esto es especialmente visible en la gráfica del error
aceptable en la banda del dos al cinco porciento.

Para revisar que el algoritmo funciona correctamente en velocidades negativas
se le ha hecho una prueba con una velocidad de referencia negativa, que ha
concluido con éxito. Se puede ver el resultado en la \autoref{fig:ch_5:negative}

Finalmente, se ha comprobado que es sistema responde bien ante cambios en la
referencia, realizando un escalón positivo y uno negativo. El resultado se
puede en la \autoref{fig:ch_5:change}. De esta figura lo más destacable es que
se nota mucho cuándo comienza el cambio una vez que se entra en la banda de
cambio aceptable ($30\%$ de la velocidad de referencia).

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/Reg_perm
	}
	\caption{
		Velocidades en régimen permanente con distintas tasas de error permitido.
	}\label{fig:ch_5:Reg_perm}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/negative
	}
	\caption{
		Velocidades negativas en régimen permanente.
	}\label{fig:ch_5:negative}
\end{figure}



\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/change
	}
	\caption{
		Cambios en la referencia de velocidad.
	}\label{fig:ch_5:change}
\end{figure}

\subsection{Conclusión}
Este algoritmo considera dos aspectos de cómo se toman las medidas. La
precisión y el tiempo de refresco. Intenta alcanzar un compromiso entre los
dos, no permitiendo valores extremos del tiempo e intentando estar a un
intervalo de error aceptable. Esta doble optimización da una cierta estabilidad
a las medidas, que suelen converger rápidamente.

Por otra parte, el compromiso entre las variables nos obliga siempre a tener un
error en régimen permanente, que podría llegar a ser eliminado con la
suposición de que las referencias son siempre de orden cero
(escalones y constantes). Además, la estabilidad en los transitorios
está sobrevalorada por este algoritmo, penalizando el comportamiento en el
permanente. La inercia del motor es lo suficientemente grande como para
garantizar que no haya comportamientos por encima de cierta frecuencia. Este
hecho se puede explotar para realizar cambios acotados del tiempo entre
interrupciones durante los periodos transitorios.

\section{Adaptación sin compromiso}\label{sec:ch_5:Adaptación_sin_compromiso}

Este algoritmo también está basado en el descenso por gradiente, salvo por dos
excepciones. La primera excepción es que se limita el tiempo entre
interrupciones durante los transitorios, no pudiendo ser arbitrariamente grande
o chico. Durante ellos se intenta que el error esté en una banda de
$[0.5, 1]\%$ de error.

La otra excepción es durante el régimen permanente. Cuando se llega al régimen
permanente se maximiza el tiempo entre interrupciones. De esta forma el
error es el mínimo posible en ese periodo.

Para minimizar el error en el régimen permanente, lo primero es establecer si
el motor está en régimen permanente en primer lugar. A este propósito se
comprueba si está en una banda del $5\%$ de
la velocidad de referencia. Si sí lo está, entonces pone el tiempo entre
interrupciones a \tt{MAX\_TIMEOUT}, que está calculado para tardar
$20~\si{\milli\second}$. Por experimentación se sabe que el tiempo que tarda el
motor desde el arranque hasta el $100\%$ de su velocidad de sincronismo es
en torno a $2~\si{\second}$. $20~\si{\milli\second}$ es un tiempo razonable en
el que se puede permitir no detectar perturbaciones, especialmente si se
corrige el tiempo rápidamente para aumentar el número de mediciones una vez que
se detecta que no se está en régimen permanente.

En caso de que se encuentre en régimen transitorio, el algoritmo es igual al de
la parte adaptativa del algoritmo propuesto en la sección anterior. Si se
compara las líneas $42$ a $61$ del \autoref{lst:ch_5:adapt_1} con las líneas
$37$ a $56$ del \autoref{lst:ch_5:adapt_2}, se puede comprobar que son idénticas a excepciones de comprobar la
variable \tt{is\_ready}, que en este nuevo programa es conocidamente
verdadera, y, por tanto, no es necesario que forme parte de las condiciones.

\lstinputlisting[language=c, caption=Interrupción en el algoritmo adaptativo
	sin compromiso, label=lst:ch_5:adapt_2, firstline=49,lastline=116]{
	ch_5/figures/PTC5F_eqep2.c}

Otra diferencia que se puede encontrar con el algoritmo anterior es que el
mínimo tiempo en el transitorio es menor que en el caso anterior. Este tiempo
mínimo de $50~\si{\micro\second}$ se corresponde con la frecuencia del PWM a la
que actúa el bucle principal de control de corrientes. Dada la estructura de
control, en la que hay un bucle externo de control de velocidad y un bucle
interno de control de corriente, no tiene sentido actualizar la medida de la
velocidad más rápidamente que las corrientes.

Normalmente, el tiempo entre interrupciones será mayor que el mínimo, ya que
actualizando tan rápidamente, el error será considerable, salvo a velocidades
muy altas. En concreto, para que con la frecuencia de CPU actual el error sea
del $1\%$, la referencia de velocidad debería ser a $12000~\si{rpm}$, velocidad
inalcanzable para el motor sin sufrir daños severos. El algoritmo aumentará el
tiempo entre interrupciones en cuanto supere el $1\%$ de error.

\subsection{Experimentos}
Se ha realizado una bancada de experimentos con este algoritmo, para comprobar
que funciona correctamente y con el objetivo de poder compararlo con el
algoritmo anterior.

En los experimentos llevados a cabo en esta prueba, no se ha modificado el
rango de error aceptable, ya que el objetivo es minimizar el error utilizando
la dinámica mecánica del motor. El error se ha forzado a estar en el rango del
$0.5\%$ al $1\%$ durante el transitorio. Si el tiempo entre interrupciones
necesarias para mantener el error dentro de esta banda es mayor a
$5~\si{\milli\second}$, entonces se establece un tiempo fijo en lugar de
intentar mantener el error en la banda mencionada.

El resultado del primer experimento se puede ver en la
\autoref{fig:ch_5:Reg_perm2}. Se observa que en régimen permanente el rizado es
casi nulo. Esto era imposible con el algoritmo anterior.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/Reg_perm2	}
	\caption{
		Distintas velocidades en régimen permanente.
	}\label{fig:ch_5:Reg_perm2}
\end{figure}

En el segundo experimento, se ha llevado a cabo una prueba para verificar si el
algoritmo afecta las características del periodo transitorio. Para lograr esto,
se ha programado un salto del $20\%$ al $25\%$ de la velocidad nominal del
motor, que equivale a un escalón en la referencia de $20.94~\rps$ a
$26.18~\rps$. El intervalo que se considera como régimen permanente es
$\left[24.87, 27.489\right]~\rps$. Se ha seleccionado este escalón en particular
debido a que, al asumir un cambio pequeño, el motor pasa una mayor proporción
del tiempo dentro del intervalo de régimen permanente en comparación con los
saltos más grandes.

En la \autoref{fig:ch_5:change_compare} se puede ver que no afecta ni al tiempo
de establecimiento, ni a la sobreoscilación ni al tiempo de retraso de forma
significativa, aunque durante un tiempo el algoritmo confunde parte del
transitorio como parte del régimen permanente. Esto se puede afinar ajustando
el parámetro \tt{MARGIN}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/change_compare
	}
	\caption{
		Comparación de propiedades.
	}\label{fig:ch_5:change_compare}
\end{figure}

Finalmente, se ha hecho una prueba más para comprobar como responde el sistema
ante cambios mayores y con cambios de dirección. Los cambios de referencia han
sido del $20\%$ al $75\%$ y del $30\%$ al $-30\%$. En la
\autoref{fig:ch_5:change_2} se pueden ver los resultados del experimento. Hay
que remarcar que para obtener los datos en este último experimento se ha
submuestreado el registro de datos. Esto implica la eliminación de muestras de
la señal, con el objetivo de reducir el tamaño de los datos. Esto tiene la
contrapartida de que se pierde información, actuando como un filtro de paso de
baja. Pero, dadas las restricciones del DSP, es necesario para observar periodos
largos de la señal.

En estas figuras se puede apreciar que a pesar de los cambios de referencia en
el transitorio, el motor sigue siendo estable durante cambios grandes de la
referencia, sin que afecte la dirección o la velocidad. La dinámica mecánica
del motor es lo suficientemente lenta como para que los cambios en la forma de
medir no tengan ningún efecto mayor sobre la controlabilidad del motor.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{
		ch_5/figures/change_2
	}
	\caption{
		Cambios en la referencia de velocidad.
	}\label{fig:ch_5:change_2}
\end{figure}

\FloatBarrier{}

\subsection{Conclusión}

Este algoritmo tiene dos objetivos distintos dependiendo del régimen en el que
se encuentre. Si está en régimen permanente, trata de minimizar el error,
tomando las medidas cada $20~\si{\milli\second}$. A distintas velocidades del
motor esto supone distintas tasas de error, pero siempre es la mínima aceptable.
A velocidades menores no es aceptable disminuir la frecuencia de las medidas
para reducir el error. Eso impediría observar comportamientos en frecuencias en
las que se conoce que hay comportamientos mecánicos del motor.

En caso de que se encuentre en régimen transitorio, el algoritmo trata de que
el error esté en una banda del $1\%$ al $0.5\%$ siempre que se tenga una
frecuencia de medida superior a $200~\si{\hertz}$. Esta frecuencia es
muy superior a la frecuencia de todos los comportamientos mecánicos conocidos
del motor.

Pero, ¿y si se pudiera prescindir de las interrupciones? Las interrupciones
tienen la necesidad de no requerir mucho tiempo de cálculo, lo que limita mucho
el posible tratamiento de la señal de la velocidad. Si se pudiera obtener la
medida en el bucle principal de control, se podría procesar la medida,
mejorándola con métodos estadísticos o filtrando componentes indeseadas de la
misma.

\ifdefined\FloatBarrier{}
	\FloatBarrier{}
\fi
